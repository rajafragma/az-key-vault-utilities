package com.inmobi.keyvault.util;

import com.microsoft.azure.PagedList;
import com.microsoft.azure.keyvault.KeyVaultClient;
import com.microsoft.azure.keyvault.models.SecretBundle;
import com.microsoft.azure.keyvault.models.SecretItem;
import org.joda.time.DateTime;

import java.util.*;

/**
 * Created by umar on 16/2/18.
 */

public class KeyVaultHelper {

    private KeyVaultClient client;
    private String keyVaultUrl;

    public KeyVaultHelper(KeyVaultClient client,String keyVaultUrl){
        this.client = client;
        this.keyVaultUrl = keyVaultUrl;
    }

    /*
    * get secret with key name
    * returns the latest version of the Secret
    * */
    public SecretBundle getSecret(String secretName){
        SecretBundle secret = client.getSecret(this.keyVaultUrl, secretName);
        return secret;
    }

    /*
   * get secret with key name and version
   * returns the key of the specified version
   * */
    public SecretBundle getSecret(String secretName,String version){
        SecretBundle secret = client.getSecret(this.keyVaultUrl, secretName,version);
        return secret;
    }

    /**
     * Gets all versions of secretName from the vault for last n days
     */
    @Deprecated
    public List<String> getAllVersionsForSecret(String secretName,Integer days){
        List<String> versions = new ArrayList<>();
        PagedList<SecretItem> secretItems = client.listSecretVersions(this.keyVaultUrl, secretName);
        secretItems.loadAll();
        Iterator<SecretItem> iterator = secretItems.iterator();
        while (iterator.hasNext()){
            SecretItem secretItem = iterator.next();
            if(isAfter(days, secretItem))
                versions.add(secretItem.identifier().version());
        }
        return versions;
    }

    /**
     * Gets all Active Secrets in ascending order(i.e last in list is the current version of secret)
     */
    public List<SecretAttributes> getAllActiveVersionsForSecret(String secretName){

        List<SecretItem> secretItemList = new ArrayList<>();
        List<SecretAttributes> secrets = new ArrayList<>();

        PagedList<SecretItem> secretItems = client.listSecretVersions(this.keyVaultUrl, secretName);
        secretItems.loadAll();

        secretItems.listIterator().forEachRemaining(s ->{
                    if(s.attributes().enabled()) {
                        secretItemList.add(s);
                    }
            }
        );

        Collections.sort(secretItemList, Comparator.comparing(t -> t.attributes()
                .created()
                .toDate()));

        secretItemList.forEach(s->{
            String version = s.identifier().version();
            secrets.add(new SecretAttributes(getSecret(secretName, version).value(),version));
        });
        return secrets;
    }

    private boolean isAfter(Integer days, SecretItem secretItem) {
        DateTime now = new DateTime();
        DateTime dateAfterWhichValuesShouldBeObtained = now.plusDays(-days);
        DateTime localDateTime = secretItem.attributes().created().toDateTime();
        return localDateTime.isAfter(dateAfterWhichValuesShouldBeObtained.getMillis());
    }


    /**
     * Returns current secret's name and version
     */
    public SecretAttributes getSecretWithCurrentVersion(String secretName){
        SecretBundle secret = getSecret(secretName);
        return new SecretAttributes(secret.value(),secret.secretIdentifier().version());
    }

    public class SecretAttributes {
        private String value;
        private String version;

        public SecretAttributes(String value, String version) {
            this.value = value;
            this.version = version;
        }

        public String getValue() {
            return value;
        }

        public String getVersion() {
            return version;
        }
    }
}
