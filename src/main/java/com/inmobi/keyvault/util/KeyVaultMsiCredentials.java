package com.inmobi.keyvault.util;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.microsoft.azure.keyvault.authentication.KeyVaultCredentials;
import okhttp3.*;

import java.io.IOException;

/**
 * Created by umar on 17/4/18.
 */
public class KeyVaultMsiCredentials extends KeyVaultCredentials{

    private String URL;
    private static final String DEFAULT_URL = "http://169.254.169.254/metadata/identity/oauth2/token";
    private static final String API_VERSION = "2018-02-01";


    public KeyVaultMsiCredentials(){
        this(DEFAULT_URL);
    }

    public KeyVaultMsiCredentials(String url){
        this(url,API_VERSION);
    }

    public KeyVaultMsiCredentials(String url,String apiVersion){
        this.URL = url+"?api-version="+apiVersion;
    }

    @Override
    public String doAuthenticate(String authorization, String resource, String scope) {
        return getMsiToken(resource);
    }

    private String getMsiToken(String resource) {
        OkHttpClient client = new OkHttpClient();

        final String url = this.URL + "&resource=" + resource;
        Request request = new Request.Builder()
                .url(url)
                .addHeader("metadata", "true")
                .addHeader("cache-control", "no-cache")
                .build();
        try {
            Response response = client.newCall(request).execute();
            Gson gson = new Gson();
            final MSIResult msiResult = gson.fromJson(response.body().string(), MSIResult.class);
            return msiResult.getAccessToken();
        }catch (IOException | JsonSyntaxException e){
            throw new RuntimeException("Unable to get Token from MSI: "+e.getMessage(), e);
        }
    }

    public static class MSIResult {
        @Expose
        @SerializedName("access_token")
        private String accessToken;
        @Expose
        @SerializedName("expires_in")
        private String expiresIn;
        @Expose
        @SerializedName("expires_on")
        private String expiresOn;
        @Expose
        @SerializedName("not_before")
        private String notBefore;
        @Expose
        @SerializedName("resource")
        private String resource;
        @Expose
        @SerializedName("token_type")
        private String tokenType;


        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public String getExpiresIn() {
            return expiresIn;
        }

        public void setExpiresIn(String expiresIn) {
            this.expiresIn = expiresIn;
        }

        public String getExpiresOn() {
            return expiresOn;
        }

        public void setExpiresOn(String expiresOn) {
            this.expiresOn = expiresOn;
        }

        public String getNotBefore() {
            return notBefore;
        }

        public void setNotBefore(String notBefore) {
            this.notBefore = notBefore;
        }

        public String getResource() {
            return resource;
        }

        public void setResource(String resource) {
            this.resource = resource;
        }

        public String getTokenType() {
            return tokenType;
        }

        public void setTokenType(String tokenType) {
            this.tokenType = tokenType;
        }

    }
}
