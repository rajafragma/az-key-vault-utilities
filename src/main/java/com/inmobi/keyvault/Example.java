package com.inmobi.keyvault;

import com.inmobi.keyvault.util.AzureKeyVaultCredential;
import com.inmobi.keyvault.util.KeyVaultHelper;
import com.inmobi.keyvault.util.KeyVaultMsiCredentials;
import com.microsoft.azure.keyvault.KeyVaultClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Created by umar on 17/4/18.
 */
public class Example {
    private static Logger LOG = LoggerFactory.getLogger(Example.class);

    public static void main(String[] args) {
        illustrateKVClientUsingMSI();
        illustrateKVClientUsingCredentials();
    }

    private static void illustrateKVClientUsingCredentials() {
        //KeyVaultClient using Service Principal ID and Key
        KeyVaultClient keyVaultSpClient = new KeyVaultClient(
                new AzureKeyVaultCredential("clientId","clientKey"));
        KeyVaultHelper keyVaultHelper = new KeyVaultHelper(keyVaultSpClient,"https://inmobikeyvault.vault.azure.net/");
        final String secretName = "sample";
        final String sample = keyVaultHelper.getSecret(secretName).value();
        LOG.info("Value From KeyVault via credentials for SecretName {}: {}", secretName, sample);
    }

    private static void illustrateKVClientUsingMSI() {
        //KeyVaultClient using MSI
        KeyVaultClient keyVaultMsiClient = new KeyVaultClient(new KeyVaultMsiCredentials());
        KeyVaultHelper keyVaultHelper = new KeyVaultHelper(keyVaultMsiClient,"https://inmobikeyvault.vault.azure.net/");
        final String secretName = "sample";
        final String sample = keyVaultHelper.getSecret(secretName).value();
        LOG.info("Value From KeyVault via MSI for SecretName {}: {}", secretName, sample);
    }
}
