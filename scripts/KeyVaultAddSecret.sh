#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

usage() { echo "Usage: $0 -s <subscriptionId> -n <keyvault_name> -k <secretName> -v <secretValue> -e <expires>" 1>&2; exit 1; }
declare subscriptionId=""
declare keyvault_name=""
declare secretName=""
declare secretValue=""
declare expires=""

while getopts ":s:n:k:v:" arg; do
	case "${arg}" in
		s)
			subscriptionId=${OPTARG}
			;;
		n)
			keyvault_name=${OPTARG}
			;;
		k)
			secretName=${OPTARG}
			;;
		v)
			secretValue=${OPTARG}
			;;
		e)
			expires=${OPTARG}
			;;
		esac
done
shift $((OPTIND-1))

if [[ -z "$subscriptionId" ]]; then
	echo "Enter subscriptionId: "
	read subscriptionId
	[[ "${subscriptionId:?}" ]]
fi

if [[ -z "$keyvault_name" ]]; then
	echo "Enter keyvault Name : "
	read keyvault_name
	[[ "${keyvault_name:?}" ]]
fi

if [[ -z "$secretName" ]]; then
	echo "secret Name (Key): "
	read secretName
	[[ "${secretName:?}" ]]
fi

if [[ -z "$secretValue" ]]; then
	echo "secret Value : "
	read secretValue
	[[ "${secretValue:?}" ]]
fi
if [[ -z "$expires" ]]; then
	echo "expire Value in days: "
	read expires
	[[ "${expires:?}" ]]
fi

az account set --subscription $subscriptionId
#Add your secrets here
echo 'Adding Secret'
az keyvault secret set --vault-name $keyvault_name --name $secretName --value $secretValue --expires $(date '+%Y-%m-%dT%H:%M:%SZ' --date="$(date '+%Y-%m-%dT%H:%M:%SZ') +$expires days")
#az keyvault secret set --vault-name $keyvault_name --name 'secretName' --value 'secretValue'
echo 'Done'