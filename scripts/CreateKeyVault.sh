#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

usage() { echo "Usage: $0 -s <subscriptionId> -n <keyvault_name> -r <resource_group_name> -l <location> " 1>&2; exit 1; }
declare subscriptionId=""
declare keyvault_name=""
declare resource_group_name=""
declare location=""

while getopts ":s:n:r:l:" arg; do
	case "${arg}" in
		s)
			subscriptionId=${OPTARG}
			;;
		n)
			keyvault_name=${OPTARG}
			;;
		r)
			resource_group_name=${OPTARG}
			;;
		l)
			location=${OPTARG}
			;;
		esac
done
shift $((OPTIND-1))

if [[ -z "$subscriptionId" ]]; then
	echo "Enter subscriptionId: "
	read subscriptionId
	[[ "${subscriptionId:?}" ]]
fi

if [[ -z "$keyvault_name" ]]; then
	echo "Enter keyvault Name : "
	read keyvault_name
	[[ "${keyvault_name:?}" ]]
fi

if [[ -z "$resource_group_name" ]]; then
	echo "Enter Resource Group Name: "
	read resource_group_name
	[[ "${resource_group_name:?}" ]]
fi

if [[ -z "$location" ]]; then
	echo "Enter Location : "
	read location
	[[ "${location:?}" ]]
fi

az account set --subscription $subscriptionId

echo 'Creating KeyVault'
az keyvault create --name $keyvault_name --resource-group $resource_group_name --location $location