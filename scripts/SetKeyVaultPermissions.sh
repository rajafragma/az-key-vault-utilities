#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

usage() { echo "Usage: $0 -s <subscriptionId> -n <keyvault_name> -i <appid> -p <permissions> " 1>&2; exit 1; }
declare subscriptionId=""
declare keyvault_name=""
declare appid=""
declare permissions=""

while getopts ":s:n:i:p:" arg; do
	case "${arg}" in
		s)
			subscriptionId=${OPTARG}
			;;
		n)
			keyvault_name=${OPTARG}
			;;
		i)
			appid=${OPTARG}
			;;
		p)
			permissions=${OPTARG}
			;;
		esac
done
shift $((OPTIND-1))

if [[ -z "$subscriptionId" ]]; then
	echo "Enter subscriptionId: "
	read subscriptionId
	[[ "${subscriptionId:?}" ]]
fi

if [[ -z "$keyvault_name" ]]; then
	echo "Enter keyvault Name : "
	read keyvault_name
	[[ "${keyvault_name:?}" ]]
fi

if [[ -z "$appid" ]]; then
	echo "Enter appid: "
	read appid
	[[ "${appid:?}" ]]
fi

if [[ -z "$permissions" ]]; then
	echo "Enter comma seperated permissions : "
	read permissions
	[[ "${permissions:?}" ]]
fi

az account set --subscription $subscriptionId
echo 'Configuring Secret Permissions'
az keyvault set-policy --name $keyvault_name --spn $appid --secret-permissions $permissions