#Azure KeyVault Java Helper

The `com.inmobi.keyvault.Example` class demonstrates connecting to the Azure Key Vault using Credentials and MSI endpoint.

The azure provided library class `com.microsoft.azure.keyvault.KeyVaultClient` is the abstraction to talk to KeyVault.
This needs an instance of abstract class `com.microsoft.azure.keyvault.authentication.KeyVaultCredentials` object to initialize.

This utility provides following two implementations of KeyVaultCredentials:
 - `AzureKeyVaultCredentials` which takes clientId and keys to authenticate.
 - `KeyVaultMsiCredentials` makes use of MSI extension's HTTP endpoint installed on VM to get the authentication token.

Please note that `com.inmobi.keyvault.util.KeyVaultMsiCredentials` would only work in a VM which has MSI extension enabled.

The utilty also provides a `com.inmobi.keyvault.util.KeyVaultHelper` class, which provides some wrappers to get secret information and versions. 
This class needs takes an instance of `KeyVaultClient` and the KeyVault HTTP URL.

## Scripts
Included in the `scripts` folder are shell scripts for key vault. 

For Running any of the shell scripts first login into your Azure cli account using the following command.<br>
```
az login
```

####CreateKeyVault.sh
This script can be used to create an Azure KeyVault Account on the Azure Subscription.<br>
Command to run this script:
```
sh CreateKeyVault.sh -s <subscriptionId> -n <keyvault_name> -r <resource_group_name> -l <location>
```

###KeyVaultAddSecret.sh
This script can be used to Add a secret to the provided KeyVault.<br>
The `-e` means expire time for the secret in days.
```
sh KeyVaultAddSecret.sh -s <subscriptionId> -n <keyvault_name> -k <secretName> -v <secretValue> -e <expires>
```

###SetKeyVaultPermissions.sh
This script can be used to set the Permission for the specified applicationId(`appid`).
```
sh SetKeyVaultPermissions.sh -s <subscriptionId> -n <keyvault_name> -i <appid> -p <permission>
```
  